library(tidyverse)
library(ggplot2)
library(dplyr)



cb2021<-table(CrimeBoston2021$MONTH)
cb2021<-data.frame(cb2021)
cb2021<-cb2021 %>% 
  rename(
    month = Var1
  )

cb2020<-table(CrimeBoston2020$MONTH)
cb2020<-data.frame(cb2020)
cb2020<-cb2020 %>% 
  rename(
    month = Var1
  )

cb2019<-table(CrimeBoston2019$MONTH)
cb2019<-data.frame(cb2019)
cb2019<-cb2019 %>% 
  rename(
    month = Var1
  )

cb2018<-table(CrimeBoston2018$MONTH)
cb2018<-data.frame(cb2018)
cb2018<-cb2018 %>% 
  rename(
    month = Var1
  )

cb2017<-table(CrimeBoston2017$MONTH)
cb2017<-data.frame(cb2017)
cb2017<-cb2017 %>% 
  rename(
    month = Var1
  )
cb2016<-table(CrimeBoston2016$MONTH)
cb2016<-data.frame(cb2016)
cb2016<-cb2016 %>% 
  rename(
    month = Var1
  )





g<-ggplot(cb2021)
g+geom_line(aes(x=month,y=Freq,group=1))+
  geom_line(data=cb2020,aes(x=month,y=Freq,group=1),color="red")+
  geom_line(data=cb2019,aes(x=month,y=Freq,group=1),color="blue")+
  geom_line(data=cb2018,aes(x=month,y=Freq,group=1),color="green")+
  geom_line(data=cb2017,aes(x=month,y=Freq,group=1),color="pink")+
  geom_line(data=cb2016,aes(x=month,y=Freq,group=1),color="purple")

typeCrime <- function(df,typeCrime){
  df <- subset(df,grepl(typeCrime,df$OFFENSE_CODE_GROUP))
}


tc2021<-table(CrimeBoston2021$OFFENSE_CODE_GROUP)
tc2021<-data.frame(tc2021)
tc2021<-tc2021 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2021,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)






tc2020<-table(CrimeBoston2020$OFFENSE_CODE_GROUP)
tc2020<-data.frame(tc2020)
tc2020<-tc2020 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2020,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)


tc2019<-table(CrimeBoston2019$OFFENSE_CODE_GROUP)
tc2019<-data.frame(tc2019)
tc2019<-tc2019 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2019,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)


tc2018<-table(CrimeBoston2018$OFFENSE_CODE_GROUP)
tc2018<-data.frame(tc2018)
tc2018<-tc2018 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2018,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)


tc2017<-table(CrimeBoston2017$OFFENSE_CODE_GROUP)
tc2017<-data.frame(tc2017)
tc2017<-tc2017 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2017,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)


tc2016<-table(CrimeBoston2016$OFFENSE_CODE_GROUP)
tc2016<-data.frame(tc2016)
tc2016<-tc2016 %>% 
  rename(
    typeCrime = Var1
  )

g<-ggplot(tc2016,aes(x=typeCrime,y=Freq,fill=typeCrime))
g+geom_bar(stat="identity")+theme(legend.position = "right",axis.text.x = element_blank())+
  geom_text(aes(label=Freq), vjust=-0.3, size=3.5)


freqTypeCrime <- cbind(tc2021,tc2020[,2])
freqTypeCrime<-freqTypeCrime %>% 
  rename(
    "2021" = Freq
  )

freqTypeCrime<-freqTypeCrime %>% 
  rename(
    "2020" = "tc2020[, 2]"
  )

freqTypeCrime <- cbind(freqTypeCrime,tc2019[,2])
freqTypeCrime<-freqTypeCrime %>% 
  rename(
    "2019" = "tc2019[, 2]"
  )

freqTypeCrime <- cbind(freqTypeCrime,tc2018[,2])
freqTypeCrime<-freqTypeCrime %>% 
  rename(
    "2018" = "tc2018[, 2]"
  )

freqTypeCrime <- cbind(freqTypeCrime,tc2017[,2])
freqTypeCrime<-freqTypeCrime %>% 
  rename(
    "2017" = "tc2017[, 2]"
  )


